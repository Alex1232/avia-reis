from django.apps import AppConfig


class BuyTicketConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'avia_base.buy_ticket'
