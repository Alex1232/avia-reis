
from django import forms


class AviaFormSearch(forms.Form):
    FROM_COUNTRY_CHOICES = [
        ('c45', 'Чебоксары'),
    ]
    WHERE_COUNTRY_CHOICES = [
        ('c02', 'Санкт-Петербург'),
        ('c04', 'Белгород'),
        ('c05', 'Иваново'),
        ('c06', 'Калуга'),
        ('c07', 'Кострома'),
        ('c08', 'Курск'),
        ('c09', 'Липецк'),
        ('c10', 'Орёл'),
        ('c11', 'Рязань'),
        ('c12', 'Смоленск'),
        ('c13', 'Тамбов'),
        ('c14', 'Тверь'),
        ('c15', 'Тула'),
        ('c16', 'Ярославль'),
        ('c18', 'Петрозаводск'),
        ('c19', 'Сыктывкар'),
        ('c20', 'Архангельск'),
        ('c21', 'Вологда'),
        ('c22', 'Вологда'),
        ('c23', 'Мурманск'),
        ('c25', 'Псков'),
        ('c28', 'Махачкала'),
        ('c30', 'Нальчик'),
        ('c33', 'Владикавказ'),
        ('c35', 'Краснодар'),
        ('c36', 'Ставрополь'),
        ('c37', 'Астрахань'),
        ('c38', 'Волгоград'),
        ('c39', 'Ростов-на-Дону'),
        ('c41', 'Йошкар-Ола'),
        ('c42', 'Саранск'),
        ('c43', 'Казань'),
        ('c44', 'Ижевск'),
        ('c46', 'Киров'),
        ('c48', 'Оренбург'),
        ('c49', 'Пенза'),
        ('c50', 'Пермь'),
        ('c51', 'Самара'),
        ('c53', 'Курган'),
        ('c54', 'Екатеринбург'),
        ('c55', 'Тюмень'),
        ('c56', 'Челябинск'),
        ('c57', 'Ханты-Мансийск'),
        ('c58', 'Салехард'),
        ('c62', 'Красноярск'),
        ('c66', 'Иркутск'),

    ]
    PASSENGERS_CHOICES = [
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
        ('7', '7'),
        ('8', '8'),
        ('9', '9'),
    ]
    CLASSES_CHOICES = [
        ('economy', 'Эконом'),
        ('business', 'Бизнес'),
    ]
    from_is = forms.ChoiceField(
        label='Откуда',
        choices=FROM_COUNTRY_CHOICES,
        widget=forms.Select(attrs={
            'class': 'form-control',
        })
    )
    where = forms.ChoiceField(
        label='Куда',
        choices=WHERE_COUNTRY_CHOICES,
        widget=forms.Select(attrs={
            'class': 'form-control',
        })
    )
    when = forms.DateTimeField(
        label='Когда',
        input_formats=['%Y/%m/%d %H:%M'],
        widget=forms.DateTimeInput(attrs={
            'class': 'form-control',
            'type': 'date'
        })
    )
    back = forms.DateTimeField(
        label='Обратно',
        input_formats=['%Y/%m/%d %H:%M'],
        widget=forms.DateTimeInput(attrs={
            'class': 'form-control',
            'type': 'date',
        })
    )
    passengers = forms.ChoiceField(
        label='Пассажиры',
        choices=PASSENGERS_CHOICES,
        widget=forms.Select(attrs={
            'class': 'form-control',
        })
    )
    classe = forms.ChoiceField(
        label='Класс',
        choices=CLASSES_CHOICES,
        widget=forms.Select(attrs={
            'class': 'form-control',
        })
    )