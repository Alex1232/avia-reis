from django.db import models


class Ticket(models.Model):
    from_is_time = models.CharField(max_length=30)
    from_is = models.CharField(max_length=30)
    duration = models.CharField(max_length=30)
    where_is_time = models.CharField(max_length=30)
    where = models.CharField(max_length=30)

    back_from_is_time = models.CharField(max_length=30)
    back_from_is = models.CharField(max_length=30)
    back_duration = models.CharField(max_length=30)
    back_where_is_time = models.CharField(max_length=30)
    back_where = models.CharField(max_length=30)