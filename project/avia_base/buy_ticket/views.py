from django.http import JsonResponse
from django.template.response import SimpleTemplateResponse
from django.views.generic import FormView, ListView

from avia_base.avia_api import action_with_data
from avia_base.buy_ticket.form import AviaFormSearch
from avia_base.buy_ticket.models import Ticket


class AviaSearch(FormView):
    template_name = 'avia/search.html'
    form_class = AviaFormSearch
    success_url = '/list'


def buy_ticket(
    form_data,
):
    del_list = ['Туда', 'Обратно', 'Купить', 'Продолжительность']
    create_list = [
        'from_is_time',
        'from_is',
        'duration',
        'where_is_time',
        'where',

        'back_from_is_time',
        'back_from_is',
        'back_duration',
        'back_where_is_time',
        'back_where',
    ]
    i = 0
    clean_list_data = []
    form_data = form_data.GET
    for data in form_data:
        if form_data[data] not in del_list:
            clean_list_data.append(
                {
                    create_list[i]: form_data[data].strip()
                }
            )
            i += 1
    ticket = Ticket.objects.create(
                from_is_time=clean_list_data[0]['from_is_time'],
                from_is=clean_list_data[1]['from_is'],
                duration=clean_list_data[2]['duration'],
                where_is_time=clean_list_data[3]['where_is_time'],
                where=clean_list_data[4]['where'],

                back_from_is_time=clean_list_data[5]['back_from_is_time'],
                back_from_is=clean_list_data[6]['back_from_is'],
                back_duration=clean_list_data[7]['back_duration'],
                back_where_is_time=clean_list_data[8]['back_where_is_time'],
                back_where=clean_list_data[9]['back_where'],
        )
    return JsonResponse(ticket.id, safe=False)


def delete_ticket(form):
    pk = form.GET['id']
    Ticket.objects.get(pk=pk).delete()
    id = pk
    return JsonResponse(id, safe=False)


def api_avia_data(request):
    from_is = request.GET['from_is']
    where = request.GET['where']
    when = request.GET['when']
    back = request.GET['back']
    passengers = request.GET['passengers']
    classe = request.GET['classe']

    response_data = {
        'results': action_with_data(from_is, where, when, back, passengers, classe)
    }

    if not response_data['results']:
        response_data = {
            'results': {
                'error': 'К сожелению авибилеты не найдены'
            }
        }

    obj_template = SimpleTemplateResponse(
        template='avia/list.html',
        context=response_data
    )

    template = obj_template.resolve_template(obj_template.template_name)
    context = obj_template.resolve_context(obj_template.context_data)
    html = template.render(context, obj_template._request)

    return JsonResponse(html, safe=False)


class AviaList(ListView):
    model = Ticket


class RootList(FormView):
    template_name = 'avia/scoreboard.html'
    form_class = AviaFormSearch
    success_url = '/root'