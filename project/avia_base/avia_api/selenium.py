from time import sleep
from typing import NoReturn

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.webdriver import WebDriver


def get_url_for_parser(from_is, where, when, back, passengers, classe) -> str:
    """Метод получает url адрес из settings"""
    return f'https://travel.yandex.ru/avia/search/result/?adult_seats={passengers}&children_seats=0&fromId={from_is}&'\
           f'infant_seats=0&klass={classe}&oneway=2&return_date={back}&toId={where}&when={when}#empty'


def get_element_by_silenium(driver, type_element: By, name_element: str):
    """Метод отдает элемент браузера"""
    return driver.find_element(type_element, name_element)


def get_html(driver) -> WebDriver:
    """Метод возвращает html страницу"""
    sleep(5)
    return driver.page_source


def create_options() -> webdriver.ChromeOptions():
    """Метод создает option для Chrome (чтобы не инициировать открытие браузера)"""
    option = webdriver.ChromeOptions()
    option.add_argument("start-maximized")
    option.add_argument("disable-infobars")
    option.add_argument("--disable-extensions")
    option.add_argument("--disable-blink-features=AutomationControlled")
    option.add_argument('--headless')
    return option


def create_web_driver(option: webdriver.ChromeOptions()) -> WebDriver:
    """Метод создает объект браузера"""
    # Настройки для неполной загрузки страницы
    # caps = DesiredCapabilities().CHROME
    # caps["pageLoadStrategy"] = "eager"
    return webdriver.Chrome(executable_path=ChromeDriverManager().install(), chrome_options=option,
                            # desired_capabilities=caps
                            )


def record_html_in_driver_by_url(driver: WebDriver, url: str) -> NoReturn:
    """Метод записывает html страницу в объект WebDriver"""
    driver.get(url)
    # Жду чтобы все прогрузилось