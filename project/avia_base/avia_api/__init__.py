from typing import (List, Dict)

from bs4 import BeautifulSoup
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.webdriver import WebDriver

from avia_base.avia_api.beautiful_soup import get_card_wrapper, get_list_parsed_data
from avia_base.avia_api.selenium import get_url_for_parser, create_options, create_web_driver, \
    record_html_in_driver_by_url, get_html


def get_html_with__silenium(from_is, where, when, back, passengers, classe) -> WebDriver:
    url: str = get_url_for_parser(from_is, where, when, back, passengers, classe)
    option: Options = create_options()
    driver: WebDriver = create_web_driver(option)

    record_html_in_driver_by_url(driver, url)

    html = get_html(driver)

    driver.quit()
    return html


def processing_html_with__beautiful_soup(html) -> List[Dict[str, str]]:
    soup = BeautifulSoup(html, 'lxml')
    product_card_wrapper: list = get_card_wrapper(soup)
    return get_list_parsed_data(product_card_wrapper)


def action_with_data(from_is, where, when, back, passengers, classe) -> List[Dict[str, str]]:
    html: WebDriver = get_html_with__silenium(from_is, where, when, back, passengers, classe)
    return processing_html_with__beautiful_soup(html)