from typing import List, Dict

from avia_base.buy_ticket.models import Ticket


def get_card_wrapper(soup):
    return soup.find_all('div', {'class': 'wydDK RB5ux f84Nt Gzd2X _cCvb'})


def get_list_parsed_data(product_card_wrapper) -> List[Dict[str, str]]:
    """
    Общий метод класса - отдает список словарей, который
    содержит в себе информацию о товаре в формате {ключ-значение}
    """
    list_parser_data = []
    _id = 0
    for product_card in product_card_wrapper:
        from_time = []
        from_data = []

        times = product_card.find_all('div', {'class': 'rC02U x45MM W8yYa P0ZeS NprJI tLvM5'})
        for time in times:
            from_time.append(time.find('span', {'class': 'tKp9d ryrs1 zoOHw ug40O'}).text),
            from_time.append(time.find('span', {'class': 'ryrs1 zoOHw ug40O'}).text),
            from_time.append(time.find('span', {'class': 'VIfp0 _h0uR dMr9m TtnCV'}).text),

        datas = product_card.find_all('div', {'class': '_dhHj _h0uR dMr9m ug40O'})
        for data in datas:
            from_data.append(data.text)

        if not Ticket.objects.filter(
                from_is_time=from_time[0],
                from_is=from_data[0],
                duration=from_time[2],
                where_is_time=from_time[1],
                where=from_data[1],

                back_from_is_time=from_time[3],
                back_from_is=from_data[2],
                back_duration=from_time[5],
                back_where_is_time=from_time[4],
                back_where=from_data[3],
        ):

            _id += 1
            list_parser_data.append(
                    {
                        'id': _id,
                        'btn': f'{_id}_btn',

                        'from_is': from_data[0],
                        'where':  from_data[1],
                        'from_is_time': from_time[0],
                        'where_is_time': from_time[1],
                        'duration': from_time[2],



                        'back_from_is': from_data[2],
                        'back_where': from_data[3],
                        'back_from_is_time': from_time[3],
                        'back_where_is_time': from_time[4],
                        'back_duration': from_time[5],
                    }
    )
    return list_parser_data