from django.contrib import admin
from django.urls import path

from avia_base.buy_ticket.views import (
    AviaSearch,
    api_avia_data,
    buy_ticket,
    AviaList,
    delete_ticket,
    RootList
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', AviaSearch.as_view()),
    path('list_reis/', api_avia_data),
    path('buy_ticket/', buy_ticket),
    path('list_buy_ticket/delete_ticket/', delete_ticket),
    path('list_buy_ticket/', AviaList.as_view(), name='list_ticket'),
    path('root/', RootList.as_view(), name='root'),
]